{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials: Data validation"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Validation\n",
    "\n",
    "Model users and model developers are usually distinct persons. Therefore, users may not be aware of model limitations. Moreover, only a few model parameters may be meaningful to them. \n",
    "\n",
    "To address these issues, **CoSApp** allows model developers to specify the validity range and visibility scope of all variables.\n",
    "This section focuses on the validation feature.\n",
    "\n",
    "## The concept\n",
    "\n",
    "A validity range and a limit range can be defined on all variables in **CoSApp**.\n",
    "\n",
    "![gauge](images/validity.svg)\n",
    "\n",
    "The validity range defines an interval within which the model can be used with confidence. The yellow areas between validity range and limits define values for which validation by an expert is required. Finally, values beyond the limits should be avoided as the reliability of the model is unknown.\n",
    "\n",
    "These ranges are only provided as information, and are not enforced during the execution of a model. Therefore, validation should be performed *a posteriori*. This behavior has been chosen to limit the constraints on mathematical systems within a model.\n",
    "\n",
    "## Defining validation criteria\n",
    "\n",
    "**CoSApp** variables are defined in `Port` or in `System` instances. Validity ranges can be specified in the `setup` of these classes.\n",
    "\n",
    "For example in a `Port`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "import logging\n",
    "logging.getLogger().setLevel(logging.INFO)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System, Port\n",
    "\n",
    "class MyPort(Port):\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_variable(\n",
    "            'v', 22.,\n",
    "            valid_range = (-2, 5),\n",
    "            invalid_comment = \"Design rule abc forbids `v` outside [-2, 5]\",\n",
    "            limits = (-10, None),\n",
    "            out_of_limits_comment = \"The model has not been tested for v < -10\",\n",
    "        )\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And in a `System`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MySystem(System):\n",
    "    \n",
    "    def setup(self):\n",
    "        # Definition of validation criteria on a inward variable 'd'\n",
    "        self.add_inward(\n",
    "            'd', 7., \n",
    "            valid_range = (-2, 5),\n",
    "            invalid_comment = \"Design rule abc forbid `d` outside [-2, 5]\",\n",
    "            limits = (None, 10),\n",
    "            out_of_limits_comment = \"The model has not been tested for d > 10\",\n",
    "        )\n",
    "        \n",
    "        # Overwrite the default validation criteria on the variable 'v' of port 'port_in'\n",
    "        self.add_input(\n",
    "            MyPort,\n",
    "            'port_in', {\n",
    "                'v': dict(\n",
    "                    valid_range = (0, 3),\n",
    "                    invalid_comment = \"Design rule blah-blah recommends `v` to be within [0, 3]\",\n",
    "                    limits = (None, 10),\n",
    "                    out_of_limits_comment = \"The model has not been tested for v > 10\"\n",
    "                )\n",
    "            }\n",
    "        )"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If no range are provided, the variable is always considered as valid. In case the value is not valid but falls within the limits, the `invalid_comment` will be shown to inform the user on the reason behind the validity range. If the value is out of limits, the `out_of_limits_comment` will be displayed.\n",
    "\n",
    "If one end of the range is unbounded, users may specify a `None` value. For example, a non-negative variable will correspond to `limits = (0, None)`.\n",
    "\n",
    "## Displaying validation criteria\n",
    "\n",
    "You can get relevant information by displaying the documentation of the `Port` or `System` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.tools import display_doc\n",
    "\n",
    "display_doc(MyPort)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display_doc(MySystem)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing validation criteria\n",
    "\n",
    "As mentioned earlier, validity ranges are not enforced during simulations. To check the validity of the results after execution, you may add special driver `ValidityCheck` to the master `System`, which will produce a post-run data validity report.\n",
    "\n",
    "Values falling outside the prescribed limits will be gathered in the `ERROR` section; those between the valid range and the limits will appear in the `WARNING` section. Valid values will simply be omitted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import ValidityCheck\n",
    "\n",
    "s = MySystem('master_system')\n",
    "\n",
    "s.add_driver(ValidityCheck('validation'))\n",
    "s.run_drivers()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example\n",
    "\n",
    "We will demonstrate this concept on the [circuit example](aa-SimpleCircuit.ipynb).\n",
    "\n",
    "The physical problem consists in determining the two node voltages which satisfy electric current conservation.\n",
    "\n",
    "![simple-circuit](images/simple_circuit.svg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System, Port, Scope\n",
    "from cosapp.drivers import NonLinearSolver, ValidityCheck\n",
    "import numpy as np"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set validity criteria\n",
    "\n",
    "By default, the current intensity will be defined as positive."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "class Voltage(Port):\n",
    "    def setup(self):\n",
    "        self.add_variable('V', unit='V')\n",
    "\n",
    "\n",
    "class Intensity(Port):\n",
    "    def setup(self):\n",
    "        self.add_variable('I',\n",
    "            unit='A',\n",
    "            limits=(0., None),\n",
    "            out_of_limits_comment=\"Current can only flow in one direction.\",\n",
    "        )"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In `Resistor` components, we add a validity range on current intensity, pretending a non-constant behaviour of the resistance value for high currents."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "import abc\n",
    "\n",
    "class Dipole(System):\n",
    "    \"\"\"Abstract directional dipole model computing\n",
    "    current from end values of electric potential.\n",
    "    \"\"\"\n",
    "    def setup(self):\n",
    "        self.add_input(Voltage, 'V_in')\n",
    "        self.add_input(Voltage, 'V_out')\n",
    "        \n",
    "        self.add_output(Intensity, 'I')\n",
    "        self.add_outward('deltaV', unit='V')\n",
    "    \n",
    "    def compute(self):\n",
    "        self.deltaV = self.V_in.V - self.V_out.V\n",
    "        self.compute_I()\n",
    "\n",
    "    @abc.abstractmethod\n",
    "    def compute_I(self) -> None:\n",
    "        pass\n",
    "\n",
    "\n",
    "class Resistor(Dipole):\n",
    "    \n",
    "    def setup(self, R=1.0):\n",
    "        super().setup()\n",
    "        \n",
    "        self.add_inward('R', R,\n",
    "            desc=\"Resistance in Ohms\",\n",
    "            limits=(0, None),\n",
    "            out_of_limits_comment=\"Resistance cannot be negative.\",\n",
    "        )\n",
    "        \n",
    "        # Add model-specific details on output I\n",
    "        I = self.I.get_details(\"I\")\n",
    "        I.valid_range = (-25, 25)\n",
    "        I.invalid_comment = \"Resistance may not be constant for currents exceeding 25 A\"\n",
    "    \n",
    "    def compute_I(self):\n",
    "        self.I.I = self.deltaV / self.R\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can look at the documentation to check that the validation criteria are taken into account"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.tools import display_doc\n",
    "display_doc(Resistor)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Building the circuit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from __future__ import annotations\n",
    "\n",
    "\n",
    "class Diode(Dipole):\n",
    "    \"\"\"Regularized diode model\n",
    "    \n",
    "    The current intensity flowing through the diode is calculated based on\n",
    "\n",
    "    $ I = I_s \\\\exp \\\\left( \\\\dfrac{V_{in} - V_{out}}{V_t} - 1 \\\\right) $\n",
    "    \"\"\"\n",
    "    tags = ['cosapp', 'developer']\n",
    "    \n",
    "    def setup(self):\n",
    "        super().setup()\n",
    "        \n",
    "        self.add_inward('Is', 1e-15, desc='Saturation current in Amps')\n",
    "        self.add_inward('Vt', 0.025875, scope=Scope.PROTECTED, desc='Thermal voltage in Volts')\n",
    "        \n",
    "    def compute_I(self):\n",
    "        \"\"\"Regularized diode model\"\"\"\n",
    "        self.I.I = self.Is * np.exp(self.deltaV / self.Vt - 1.)\n",
    "\n",
    "\n",
    "class Node(System):\n",
    "    \"\"\"Electric node model with `n_in` incoming and `n_out` outgoing currents.\n",
    "    \"\"\"\n",
    "    def setup(self, n_in=1, n_out=1):\n",
    "        self.add_property('n_in', max(1, int(n_in)))\n",
    "        self.add_property('n_out', max(1, int(n_out)))\n",
    "\n",
    "        incoming = tuple(\n",
    "            self.add_input(Intensity, f\"I_in{i}\")\n",
    "            for i in range(self.n_in)\n",
    "        )\n",
    "        outgoing = tuple(\n",
    "            self.add_input(Intensity, f\"I_out{i}\")\n",
    "            for i in range(self.n_out)\n",
    "        )\n",
    "        self.add_property('incoming_currents', incoming)\n",
    "        self.add_property('outgoing_currents', outgoing)\n",
    "        \n",
    "        self.add_inward('V', 1.0, unit='V')\n",
    "        self.add_outward('sum_I_in', 0., unit='A', desc='Sum of all incoming currents')\n",
    "        self.add_outward('sum_I_out', 0., unit='A', desc='Sum of all outgoing currents')\n",
    "        \n",
    "        self.add_unknown('V')\n",
    "        self.add_equation('sum_I_in == sum_I_out', name='current balance')\n",
    "\n",
    "    def compute(self):\n",
    "        self.sum_I_in = sum(current.I for current in self.incoming_currents)\n",
    "        self.sum_I_out = sum(current.I for current in self.outgoing_currents)\n",
    "\n",
    "    @classmethod\n",
    "    def make(\n",
    "        cls,\n",
    "        name: str,\n",
    "        parent: System,\n",
    "        incoming: list[Dipole]=[],\n",
    "        outgoing: list[Dipole]=[],\n",
    "        pulling=None,\n",
    "    ) -> Node:\n",
    "        \"\"\"Factory creating new node within `parent`, with\n",
    "        appropriate connections with incoming and outgoing dipoles.\n",
    "        \"\"\"\n",
    "        node = cls(name, n_in=len(incoming), n_out=len(outgoing))\n",
    "        parent.add_child(node, pulling=pulling)\n",
    "        \n",
    "        for dipole, current in zip(incoming, node.incoming_currents):\n",
    "            parent.connect(dipole.I, current)\n",
    "            parent.connect(dipole.V_out, node.inwards, 'V')\n",
    "        \n",
    "        for dipole, current in zip(outgoing, node.outgoing_currents):\n",
    "            parent.connect(dipole.I, current)\n",
    "            parent.connect(dipole.V_in, node.inwards, 'V')\n",
    "\n",
    "        return node\n",
    "\n",
    "\n",
    "class Source(System):\n",
    "    \n",
    "    def setup(self, I=0.1):\n",
    "        self.add_inward('I', I)\n",
    "        self.add_output(Intensity, 'I_out', {'I': I})\n",
    "    \n",
    "    def compute(self):\n",
    "        self.I_out.I = self.I\n",
    "\n",
    "\n",
    "class Ground(System):\n",
    "    \n",
    "    def setup(self, V=0.):\n",
    "        self.add_inward('V', V)\n",
    "        self.add_output(Voltage, 'V_out', {'V': V})\n",
    "    \n",
    "    def compute(self):\n",
    "        self.V_out.V = self.V\n",
    "        \n",
    "\n",
    "class Circuit(System):\n",
    "    \n",
    "    def setup(self):\n",
    "        R1 = self.add_child(Resistor('R1', R=100.), pulling={'V_out': 'Vg'})\n",
    "        R2 = self.add_child(Resistor('R2', R=10000.))\n",
    "        D1 = self.add_child(Diode('D1'), pulling={'V_out': 'Vg'})  \n",
    "        \n",
    "        # Define nodes\n",
    "        Node.make('n1',\n",
    "            parent=self,\n",
    "            outgoing=[R1, R2],\n",
    "            pulling={'I_in0': 'I_in'},\n",
    "        )\n",
    "        Node.make('n2',\n",
    "            parent=self,\n",
    "            incoming=[R2],\n",
    "            outgoing=[D1],\n",
    "        )\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a function generating the system of interest"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_circuit(name='model', I=1.0, add_drivers=True) -> System:\n",
    "    \"\"\"Factory creating a circuit with initial source intensity `I`.\n",
    "    If `add_drivers` is True, a numerical solver and validation driver\n",
    "    are added to the head system.\n",
    "    \"\"\"\n",
    "    s = System(name)\n",
    "\n",
    "    s.add_child(Source('source', I=I))\n",
    "    s.add_child(Ground('ground', V=0.0))\n",
    "    s.add_child(Circuit('circuit'))\n",
    "\n",
    "    s.connect(s.source.I_out, s.circuit.I_in)\n",
    "    s.connect(s.ground.V_out, s.circuit.Vg)\n",
    "\n",
    "    if add_drivers:\n",
    "        # Add numerical solver and validation driver\n",
    "        s.add_driver(NonLinearSolver('solver'))\n",
    "        s.add_driver(ValidityCheck('validation'))\n",
    "    \n",
    "    return s"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solving the valid problem\n",
    "\n",
    "First we will solve the initial problem with a intensity source of 0.1 A and check that the results are valid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = make_circuit(I=0.1)\n",
    "\n",
    "# Execute the problem\n",
    "p.run_drivers()\n",
    "\n",
    "print(f\"Sanity check: {p.circuit.R1.I.I + p.circuit.D1.I.I = } ({p.source.I} expected)\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solving invalid problems\n",
    "\n",
    "Then we will solve the problem with a high intensity in order to trigger the validation criteria on the resistors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = make_circuit(I=50.)\n",
    "\n",
    "# Execute the problem\n",
    "p.run_drivers()\n",
    "\n",
    "print(f\"Sanity check: {p.circuit.R1.I.I + p.circuit.D1.I.I = } ({p.source.I} expected)\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we will solve the problem with a intensity source of -0.1 A (regarded as invalid, here)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p = make_circuit(I=-0.1)\n",
    "\n",
    "# Execute the problem\n",
    "p.run_drivers()\n",
    "\n",
    "print(f\"Sanity check: {p.circuit.R1.I.I + p.circuit.D1.I.I = } ({p.source.I} expected)\")"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "cosapp",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.16"
  },
  "toc": {
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": "block",
   "toc_window_display": true
  },
  "vscode": {
   "interpreter": {
    "hash": "03d8647662c9fbe9220ebb6c4a5dd3c1d557fb5efab079901b8383e5f052f0cc"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
