"""
The base code for this Signal/Slot implementation comes from
source: https://github.com/Numergy/signalslot
Copyright (c) Numergy
"""

from cosapp.core.signal.signal import Signal
from cosapp.core.signal.slot import Slot

__all__ = ["Signal", "Slot"]
